import requests

URL = "http://localhost:5000"
ALLOWED_SYMBOLS = set([i for i in 'qwertyuiop[]asdfghjklzxcvbnm)(1234567890ёй цукенгшщзхъфывапролджэячсмитьбю+-_!?'])


def show_leaderboard():
    leaders = requests.get(f"{URL}/leaderboard").json()
    for num, i in enumerate(leaders):
        print(f'#{num + 1}. {i[0]} - {i[1]}')


def answer_check(correct_answers):
    word = input().lower()
    if word in correct_answers:
        return word
    else:
        print(f'Это не ответ на мой вопрос.. Ответь: {", ".join(correct_answers[:-1]) + " или " + correct_answers[-1]}')
        return answer_check(correct_answers)


def check_symbols(word):
    for i in word:
        if i not in ALLOWED_SYMBOLS:
            return 'incorrect'
    return 'correct'


def check_password(password, user_id):
    req = requests.post(f"{URL}/password_check", json={'user_id': user_id, 'password': password}).json()
    if req == 'success':
        return
    elif req == 'incorrect_symbols':
        print('Использованы недопустимые символы, введите верный пароль')
        password = input()
        return check_password(password, user_id)
    else:
        print('Пароль неверен, введите верный пароль')
        password = input()
        return check_password(password, user_id)


def make_bet(player_balance, bet_amount, player_id):
    req = requests.post(f"{URL}/make_bet", json={'player_balance': player_balance, 'bet_amount': bet_amount,
                                                 'id': player_id}).json()
    if req == 'success':
        return bet_amount
    if req == 'wrong_bet_type':
        print('Ставкой должно быть целое число, введите другую ставку')
    if req == 'not_enough_bet':
        print('Ставкой должна превышать 0 деняк, введите другую ставку')
    if req == 'not_enough_money':
        print('На балансе недосточн средств, введите другую ставку')
    return make_bet(player_balance, input(), player_id)


def registration(user_name, password):
    req = requests.post(f"{URL}/registration", json={'user_name': user_name, 'password': password}).json()
    if req['condition'] == 'incorrect_symbols':
        print('В вашем никнейме или пароле содержатся недопустимые символы')
        user_name = input('Введите новый никнейм\n')
        password = input('Введите пароль\n')
        return registration(user_name, password)
    elif req['condition'] == 'already_taken':
        user_name = input('Данное имя пользователя занято, введите новое\n')
        return registration(user_name, password)
    else:
        print('Вы были успешно зарегестрированы, зачисляю на баланс 99999 деняк')
        return 99999, req['id']


def authorization(user_name):
    req = requests.post(f"{URL}/authorization", json={'username': user_name}).json()
    if req['old_player']:
        password = input('Введите пароль\n')
        check_password(password, req['player_id'])
        print(f'С возвращением, {user_name}, твой баланс составляет {req["balance"]}')
        return req['balance'], req['player_id']
    else:
        password = input('Я смотрю ты здесь впервые, придумай пароль\n')
        reg = registration(user_name, password)
        return reg


def answer_is_right(answer):
    req = requests.post(f"{URL}/answer_is_right", json={'answer': answer}).json()
    if req == 'correct':
        return True
    return False


def add_to_player_balance(player_id):
    requests.post(f"{URL}/add_to_player_balance", json={'id': player_id})


def subtract_from_player_balance(player_id):
    requests.post(f"{URL}/subtract_from_player_balance", json={'id': player_id})


def exit_game(player_id):
    requests.post(f"{URL}/exit_game", json={'id': player_id})


def main_process():
    plays_in_a_row = 0
    player_name = input('Добро пожаловать в игру! Представься, незнакомец\n')
    if check_symbols(player_name) == 'correct':
        balance, player_id = authorization(player_name)
        print('Хочешь увидеть таблицу самых удачливых игроков? (ответь "да" или "нет")')
        answer = answer_check(['да', 'нет'])
        if answer == 'да':
            show_leaderboard()
        game_process = True
        if balance == 0:
            print('У тебя на балансе 0 деняк, а значит ты не сможешь играть! Проваливай!')
            game_process = False
        while game_process:
            plays_in_a_row += 1
            print(f'Начнём игру, {player_name}! Для начала введи сумму ставки:')
            bet = int(make_bet(balance, input(), player_id))
            if plays_in_a_row == 1:
                print('Приступим к игре! В одной из этих коробок находится красный шарик')
                print('📦 📦 📦')
                print('Если угадаешь, в какой, я удвою твою ставку; но в противном случае - заберу всё себе')
                print('А теперь выбирай: напиши мне цифру 1 до 3')
            else:
                print('📦 📦 📦')
                print('В какой же коробке шарик на этот раз? (снова пишши цифру от 1 до 3)')
            answer = answer_check(['1', '2', '3'])
            if answer_is_right(answer):
                print(f'Верно! Сейчас начислю на твой баланс {bet * 2} деняк')
                add_to_player_balance(player_id)
                balance += bet
            else:
                print('Не угадал!')
                subtract_from_player_balance(player_id)
                balance -= bet
            print(f'Теперь твой баланс - {balance} деняк')
            print('Сыграем ещё? (ответь "да", "нет" или "таблица", чтобы увидеть таблицу лидеров)')
            answer = answer_check(['да', 'нет', 'таблица'])
            if answer == 'нет':
                print(f'Спасибо за игру, {player_name}! Ещё увидимся')
                game_process = False
            elif answer == 'таблица':
                show_leaderboard()
                print('Теперь сыграем?')
                answer = answer_check(['да', 'нет'])
                if answer == 'нет':
                    print(f'Спасибо за игру, {player_name}! Ещё увидимся')
                    game_process = False
                elif balance == 0:
                    print('У тебя на балансе 0 деняк, а значит ты не сможешь играть! Проваливай!')
                    game_process = False
            elif balance == 0:
                print('У тебя на балансе 0 деняк, а значит ты не сможешь играть! Проваливай!')
                game_process = False
        exit_game(player_id)
    else:
        print('Ваше имя содержит недопустимые символы')
        main_process()


main_process()
