from flask import Flask, jsonify, request
import random
import json

app = Flask(__name__)

ALLOWED_SYMBOLS = set([i for i in 'qwertyuiop[]asdfghjklzxcvbnm)(1234567890ёй цукенгшщзхъфывапролджэячсмитьбю+-_!?'])


def check_symbols(word):
    for i in word:
        if i not in ALLOWED_SYMBOLS:
            return 'incorrect'
    return 'correct'


@app.route("/")
def index():
    return "<h1>Run client </h1>"


@app.route("/authorization", methods=["POST"])
def authorization():
    user_name = request.json['username']
    with open('data.json', 'r') as players_data_file:
        players_data = json.loads(json.load(players_data_file))['players']

    player_is_found = False
    for i in players_data:
        if i['player_name'] == user_name:
            balance = i['balance']
            player_id = i['id']
            player_is_found = True

    if player_is_found:
        return jsonify({'old_player': True,
                        'balance': balance,
                        'player_id': player_id})
    else:
        return jsonify({'old_player': False})


@app.route("/password_check", methods=["POST"])
def password_check():
    user_id = request.json['user_id']
    password = request.json['password']

    with open('data.json', 'r') as data_file:
        players_data = json.loads(json.load(data_file))['players']
    with open('currently_playing.json', 'r') as playing_users:
        currently_playing = json.loads(json.load(playing_users))

    if check_symbols(password) == 'incorrect':
        return jsonify('incorrect_symbols')

    for i in players_data:
        if i['id'] == user_id and i['password'] == password:
            currently_playing['currently_playing'].append({
                'id': user_id,
                'bet': None
            })
            with open('currently_playing.json', 'w') as cur:
                json.dump(json.dumps(currently_playing), cur)
            return jsonify('success')
        elif i['id'] == user_id and i['password'] != password:
            return jsonify('failure')


@app.route("/registration", methods=['POST'])
def registration():
    print(request.json)
    user_name = request.json['user_name']
    password = request.json['password']

    if check_symbols(user_name) == 'incorrect' or check_symbols(password) == 'incorrect':
        return jsonify({'condition': 'incorrect_symbols', 'id': None})

    with open('data.json', 'r') as players_data:
        players_data = json.loads(json.load(players_data))['players']
    with open('currently_playing.json', 'r') as playing_users:
        currently_playing = json.loads(json.load(playing_users))

    players_names = [i['player_name'] for i in players_data]
    if user_name in players_names:
        return jsonify({'condition': 'already_taken', 'id': None})

    print(players_data)

    max_id = max([i['id'] for i in players_data])
    players_data.append({
        'id': max_id + 1,
        'player_name': user_name,
        'password': password,
        'balance': 99999
    })
    currently_playing['currently_playing'].append({
        'id': max_id + 1,
        'bet': None
    })

    with open('data.json', 'w') as players_data_file:
        json.dump(json.dumps({'players': players_data}), players_data_file)
    with open('currently_playing.json', 'w') as cur:
        json.dump(json.dumps(currently_playing), cur)

    return jsonify({'condition': 'success', 'id': max_id + 1})


@app.route('/make_bet', methods=['POST'])
def make_bet():
    player_balance = request.json['player_balance']
    player_id = request.json['id']
    try:
        bet_amount = int(request.json['bet_amount'])
    except:
        return jsonify('wrong_bet_type')

    if bet_amount <= 0:
        return jsonify('not_enough_bet')
    if bet_amount > player_balance:
        return jsonify('not_enough_money')

    with open('currently_playing.json', 'r') as playing_users:
        currently_playing = json.loads(json.load(playing_users))

    for i in currently_playing['currently_playing']:
        if i['id'] == player_id:
            i['bet'] = bet_amount

    with open('currently_playing.json', 'w') as cur:
        json.dump(json.dumps(currently_playing), cur)
    return jsonify('success')


@app.route("/answer_is_right", methods=["POST"])
def answer_is_right():
    answer = request.json['answer']
    if int(answer) == random.randint(1, 3):
        return jsonify('correct')
    return jsonify('incorrect')


@app.route("/add_to_player_balance", methods=["POST"])
def add_to_player_balance():
    player_id = request.json['id']
    with open('data.json', 'r') as players_data:
        players_data = json.loads(json.load(players_data))
    with open('currently_playing.json', 'r') as playing_users:
        currently_playing = json.loads(json.load(playing_users))

    for i in currently_playing['currently_playing']:
        if i['id'] == player_id:
            bet = i['bet']

    for i in players_data['players']:
        if i['id'] == player_id:
            i['balance'] += bet
            with open('data.json', 'w') as players_data_file:
                json.dump(json.dumps(players_data), players_data_file)
            return


@app.route("/subtract_from_player_balance", methods=["POST"])
def subtract_from_player_balance():
    player_id = request.json['id']
    with open('data.json', 'r') as players_data:
        players_data = json.loads(json.load(players_data))
    with open('currently_playing.json', 'r') as playing_users:
        currently_playing = json.loads(json.load(playing_users))

    for i in currently_playing['currently_playing']:
        if i['id'] == player_id:
            bet = i['bet']

    for i in players_data['players']:
        if i['id'] == player_id:
            i['balance'] -= bet
            with open('data.json', 'w') as players_data_file:
                json.dump(json.dumps(players_data), players_data_file)
            return


@app.route("/leaderboard", methods=["GET"])
def leaderboard():
    with open('data.json', 'r') as players_data_file:
        players_data = json.loads(json.load(players_data_file))['players']
    players_data = sorted(players_data, key=lambda x: x['balance'], reverse=True)
    result = []
    for i in players_data:
        result.append((i['player_name'], i['balance']))
    print(tuple(result))
    return jsonify(tuple(result))


@app.route('/exit_game', methods=['POST'])
def exit_game():
    player_id = request.json['id']
    with open('currently_playing.json', 'r') as playing_users:
        currently_playing = json.loads(json.load(playing_users))

    currently_playing['currently_playing'] = [i for i in currently_playing['currently_playing'] if i['id'] != player_id]
    with open('currently_playing.json', 'w') as cur:
        json.dump(json.dumps(currently_playing), cur)


if __name__ == "__main__":
    app.run()
